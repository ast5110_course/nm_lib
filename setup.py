#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 30 04:40:21 2021

@author: Juan Martinez Sykora
"""

from setuptools import setup

setup(
      name='nm_lib',
      version='0.0',
      py_modules=['nm_lib'],
      author   = 'Juan Martinez Sykora',
      author_email = 'martinezsykora@baeri.org'#,
      #url = 'https://gitlab.com/XXX/nm_lib_py'
)
