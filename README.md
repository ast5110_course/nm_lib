# nm_lib

This is intendend for student to develop the library of a numerical code following the [excersices](https://gitlab.com/ast5110_course/ast5110). 

Please create [nm_lib wiki](https://gitlab.com/ast5110_course/nm_lib/-/wikis/home) to add a detailed description about this code and what it can do!

In the long run, create [CI/CD pipeline](https://gitlab.com/ast5110_course/nm_lib/-/pipelines) to warranty that each commit successfuly pass a small number of tests. 

### Fork this reposotory privately:  
[to create a fork follow the instructions](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

### To install the files:
```
cd nm_lib
python setup.py develop
```

### To start using the library:
Run this code to get started:
```
import nm_lib as nm
```
